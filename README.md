
# Wild cryptography, types, and protocols

## Code of conduct and licenses.

[![Contributor
Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)

The [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) applies.

All code is dual licensed as MIT + Apache-2.0 unless specified otherwise.

All documentation is licensed CC0 unless specified otherwise.

All references are free of patents as far as contributors are aware unless
stated otherwise.

All contributors certify their contributions under the [Developer Certificate
of Origin](https://developercertificate.org/) by signing their commits.

## Teams on Gitea

### Owners

Owners have full control over the Gitea organization. Only the owners may
create new repositories and configure the CI.

### Contributors

Contributors have signed the [Developer Certificate of
Origin](https://developercertificate.org/) by adding their name, email, and
fingerprint of their signing public key to `.mailmap`; have imported their
public key; and signed their commit updating these files.

Developers may approve a contributor but only the owners may add users to
teams. Developers may merge PRs without waiting for the owners to update the
team membership.

### Developers

Developers are a trusted subset of contributors who may merge pull requests
after CI checks have passed. All tagged releases require at least 2n/3 + 1
developers to reach consensus.

### Robots

Robots may or may not have special powers. The CI bot is unfortunately
necessarily an administrator of each repository. However, this is granted
manually and not over the organization itself.

## Signing the Developer Certificate of Origin

Before your contributions may be accepted, you must first sign the Developer
Certificate of Origin, or DCO for short. Unlike the Linux Foundation who rely
on the use of git's `--signoff` we use a more explicit stricter policy using
signed commits to represent DCO certification. We only accept signatures from
keys containing a user ID aligned with the name and email address presented by
git on every commit.

You only need to follow this process once.

These user IDs and key fingerprints are registered in the human readable
`.mailmap` and the public keys are stored in a GPG keychain, both in this
repository. The CI does not trust or query any external keyservers to update
information or to expire or revoke keys.

**NOTE:** You can run `sh ./sign-dco.sh` to automate the following.

Read the DCO.

```shell
cat DCO.txt
```

We use your name and email as recognized by git as your PGP user ID.

```shell
USERID="$(git config user.name) <$(git config user.email)>"
```

List the long key ID for your public key.

```shell
gpg -K --keyid-format long "$USERID"
KEYID="$(gpg -K --with-colons "$USERID" | grep '^fpr' | cut -d: -f10)"
```

Verify this is the right key before continuing.

If you don't already have a key; you may generate a signing-only key which
never expires.

```shell
gpg --quick-generate-key "$USERID" ed25519 sign never
```

Add your name to the `.mailmap`.

```shell
echo "$USERID # $KEYID" >> .mailmap
```

Import your public key into our keychain.

```shell
gpg --export-filter "keep-uid=uid=$USERID" --export "$KEYID" | \
    GNUPGHOME=.gnupg gpg --import
GNUPGHOME=.gnupg gpg --export | gpg --list-packets > .gnupg/textual-dump
```

Finally add these changes, commit, push to your fork, and open a PR.

```shell
git add .mailmap .gnupg
git commit -S -m "$(git config user.name) certifies the DCO"
```
