#!/bin/sh

set -eu

if command -v gpg2 > /dev/null
then
  GPG=gpg2
elif command -v gpg > /dev/null
then
  GPG=gpg
else
  echo "Couldn't find GPG!"
  exit 1
fi

if ! command -v git > /dev/null
then
  echo "Couldn't find git!"
  exit 2
fi

if ! test -f .mailmap
then
  echo "Couldn't find .mailmap! Are you calling this in /meta/?"
  exit 3
fi

if ! git config user.name > /dev/null && git config user.email > /dev/null
then
  echo "Git doesn't know who you are, therefore, neither do I."
  echo
  echo '  git config --global user.name "Jane Doe"'
  echo '  git config --global user.email "jane@doe.email"'
  exit 4
fi

USERID="$(git config user.name) <$(git config user.email)>"

if test -z "$("$GPG" -K --keyid-format long "$USERID")"
then
  echo "You do not have a PGP secret key with the user ID '$USERID'."
  echo "To generate a signing-only key which will not expire:"
  echo
  echo "  gpg --quick-generate-key '$USERID' ed25519 sign never"
  exit 5
fi

cat DCO.txt

while :
do
  # at least one result
  "$GPG" -K --keyid-format long "$USERID"

  if grep -q "$USERID" .mailmap
  then
    echo "WARNING: You have already signed the DCO!"
  fi

  echo "Hello and welcome $(git config user.name). If you agree to the DCO"
  printf "and wish to continue. Enter the 40-hex key-id for your key.\n> "
  read -r KEYID

  if test ${#KEYID} -eq 40 && "$GPG" -K --keyid-format long \
      "$USERID" | grep -q "$KEYID"
  then
    break
  else
    echo "Invalid key ID."
  fi
done

if test "$(git config commit.gpgsign)" != "true"
then
  echo "Your git is not already configured to sign commits."
  echo "If you want to enable git signing globally; run:"
  echo
  echo "  git config --global --add commit.gpgsign true"
  echo
  echo "Alternatively, you may manually sign your commits by adding"
  echo "'-S' to 'git commit'."
fi

"$GPG" --export-filter "keep-uid=uid=$USERID" --export "$KEYID" \
  | GNUPGHOME=.gnupg "$GPG" --import

GNUPGHOME=.gnupg "$GPG" --export | gpg --list-packets > .gnupg/textual-dump

echo "$USERID # $KEYID" >> .mailmap

echo "Thank you for signing the DCO. Finally add theses changes, commit,"
echo "push to your fork, and open a PR."
echo
echo "  git add .mailmap .gnupg"
echo "  git commit -S -m '$(git config user.name) certifies the DCO'"
